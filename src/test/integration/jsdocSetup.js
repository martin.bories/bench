import logger from './logger';

const log = logger.init('jsdocSetup.js');

const jsdocSetup = {
	afterAll: async() => {

	},
	beforeAll: async() => {

	},
};

export default jsdocSetup;
