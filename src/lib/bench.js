import logger from './logger';

const log = logger.init('bench.js');

/**
 * @typedef mark
 * @function
 * @returns {int} The time passed in milliseconds.
 */

/**
 * Returns a benchmarking-function.
 *
 * Calling the benchmarking-function will return the time in milliseconds since it's creation and print a benchmark in the `debug`-channel.
 * @access public
 * @augments external:log.debug
 * @augments Date.now
 * @author Martin Bories <martin.bories@megatherium.solutions>
 * @copyright 2020 Megatherium UG (haftungsbeschränkt)
 * @example <caption>Benchmark `setTimeout`</caption>
 * import assert from 'assert';
 * import bench from '@megatherium/bench';
 * import sleep from '@megatherium/sleep';
 *
 * const mark = bench('Waiting 2 seconds');
 * (async() => {
 * 	await sleep(2000);
 * 	const timeInMilliseconds = mark();
 * 	assert.strictEqual(timeInMilliseconds >= 2000, true);
 * })();
 * @example <caption>Benchmark `setTimeout` with a custom logger</caption>
 * import assert from 'assert';
 * import bench from '@megatherium/bench';
 * import logger from '@megatherium/log';
 * import sleep from '@megatherium/sleep';
 *
 * const log = logger.init('test.js', 'jsdoc-example/');
 *
 * const mark = bench('Waiting 2 seconds', log);
 * (async() => {
 * 	await sleep(2000);
 * 	const timeInMilliseconds = mark();
 * 	assert.strictEqual(timeInMilliseconds >= 2000, true);
 * })();
 * @example <caption>Benchmark without a label</caption>
 * import assert from 'assert';
 * import bench from '@megatherium/bench';
 * import sleep from '@megatherium/sleep';
 *
 * const mark = bench();
 * (async() => {
 * 	await sleep(2000);
 * 	const timeInMilliseconds = mark();
 * 	assert.strictEqual(timeInMilliseconds >= 2000, true);
 * })();
 * @license Megatherium Standard License 1.0
 * @module bench
 * @param {string=} label The label of the mark to set.
 * @param {external:log=} customLog The logger to use for printing the benchmark. Useful to print the label with the right filename.
 * @returns {mark} Returns a function which does not take parameters. Once called, it will return the time passed in milliseconds.
 */
const bench = (label, customLog) => {
	const t0 = Date.now();
	return () => {
		const t1 = Date.now(),
			dt = t1 - t0,
			prefix = label
				? `${ label }: `
				: '';

		(customLog || log).debug(`[Benchmark] ${ prefix }${ dt }ms.`);
		return dt;
	};
};

export default bench;
