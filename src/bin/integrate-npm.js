/*global __dirname,process*/
import child_process from 'child_process';
import exec from '@megatherium/exec';
import fs from '@megatherium/fs-promises';
import gitlab from '@megatherium/gitlab-api';
import hasProperty from '@megatherium/has-property';
import logger from './logger';
import mkdirp from 'mkdirp';
import ncu from 'npm-check-updates';
import npm from '@megatherium/npm-api';
import os from 'os';
import path from 'path';
import {
	promisify,
} from 'util';
import rimrafCallback from 'rimraf';
import sleep from '@megatherium/sleep';

const log = logger.init('integrate.js');

const childProcessExec = promisify(child_process.exec),
	rimraf = promisify(rimrafCallback);

const integrate = async() => {
	const parentDirectory = path.join(os.tmpdir(), 'megatherium', 'hydra-cli', 'integration'),
		projectNames = [
			'api-client',
			'bench',
			'crypto',
			'eslint-config',
			'eslint-config-component',
			'exec',
			'fs-promises',
			'has-property',
			'is-uuidv4',
			'log',
			'npm-api',
			'random',
			'rimraf',
			'rimraf-cli',
			'sleep',
			'test',
			'type',
			'type-server',
		],
		myPackageJsonFilename = path.join(__dirname, '..', '..', 'package.json'),
		myPackageJsonContent = await fs.readFile(myPackageJsonFilename),
		myPackageJson = JSON.parse(myPackageJsonContent.toString('utf-8'));

	// Get latest version.
	const latestVersion = await npm.getLatestVersion(myPackageJson.name),
		targetVersionName = `^${ latestVersion }`;
	await mkdirp('/root/.ssh');
	await childProcessExec('ssh-keyscan -t rsa gitlab.com >> ~/.ssh/known_hosts', {
		stdio: 'inherit',
	});

	// Configure git.
	await exec('git', [
		'config',
		'--global',
		'user.email',
		process.env.GITLAB_USER_EMAIL,
	]);
	await exec('git', [
		'config',
		'--global',
		'user.name',
		process.env.GITLAB_USER_NAME,
	]);
	
	// Create temporary ssh key
	const publicKeyLabel = `temporary-pipeline-key-${ process.env.CI_JOB_ID }`,
		publicKeyFilename = path.join(os.userInfo().homedir, '.ssh', 'id_rsa.pub');
	if (!await fs.exists(publicKeyFilename)) {
		await childProcessExec(`ssh-keygen -t rsa -b 2048 -C "${ publicKeyLabel }" -f ~/.ssh/id_rsa -q -N ""`, {
			stdio: 'inherit',
		});
	}
	const publicKeyContent = await fs.readFile(publicKeyFilename);
	await gitlab.createUserKey(publicKeyLabel, publicKeyContent.toString('utf-8'));
	log.debug(`Successfully created user key.`);

	// Update dependency.
	await mkdirp(parentDirectory);
	for (let i = 0; i < projectNames.length; ++i) {
		const projectName = projectNames[i];
		const project = await gitlab.request('GET', `/projects/${ gitlab.parseProjectIdOrName(`megatherium/${ projectName }`) }`),
			directory = path.join(parentDirectory, project.path),
			packageJsonFilename = path.join(directory, 'package.json');
		await rimraf(directory);
		await mkdirp(directory);
		await exec('git', [
			'clone',
			`git@gitlab.com:${ project.path_with_namespace }.git`,
			directory,
		]);
		if (!await fs.exists(packageJsonFilename)) {
			continue;
		}
		const packageJsonContent = await fs.readFile(packageJsonFilename),
			packageJson = JSON.parse(packageJsonContent.toString('utf-8'));
		let releaseType = undefined;
		if (hasProperty(packageJson, 'dependencies') && hasProperty(packageJson.dependencies, myPackageJson.name) && packageJson.dependencies[myPackageJson.name] !== targetVersionName) {
			releaseType = 'fix';
			packageJson.dependencies[myPackageJson.name] = targetVersionName;
		} else if (hasProperty(packageJson, 'devDependencies') && hasProperty(packageJson.devDependencies, myPackageJson.name) && packageJson.devDependencies[myPackageJson.name] !== targetVersionName) {
			releaseType = 'chore';
			packageJson.devDependencies[myPackageJson.name] = targetVersionName;
		}

		// Commit.
		const options = {
			cwd: directory,
			stdio: 'inherit',
		};
		if (releaseType !== undefined) {
			log.info(`${ packageJson.name }: Updating...`);
			const branchName = `update-${ myPackageJson.name.substring(myPackageJson.name.indexOf('/')+1) }`;
			await exec('git', [
				'checkout',
				'-b',
				branchName,
			], options);
			await ncu.run({
				packageFile: packageJsonFilename,
				upgrade: true,
			});
			await exec('npm', [
				'i',
			], options);
			await exec('git', [
				'add',
				'package.json',
				'package-lock.json',
			], options);
			await exec('git', [
				'commit',
				'-m',
				`${ releaseType }: Updated ${ myPackageJson.name } to ${ latestVersion }`,
			], options);
			await exec('git', [
				'push',
				'-u',
				'origin',
				branchName,
			], options);

			// Create MR.
			log.info(`${ packageJson.name }: Creating merge request...`);
			const mergeRequest = await gitlab.request('POST', `/projects/${ project.id }/merge_requests`, {
				source_branch: branchName,
				target_branch: 'master',
				title: `Updated ${ myPackageJson.name } to ${ latestVersion }`,
				remove_source_branch: true,
			});

			// Auto-merge MR.
			while (true) {
				let successful = true;
				try {
					await gitlab.request('PUT', `/projects/${ project.id }/merge_requests/${ mergeRequest.iid }/merge`, {
						merge_when_pipeline_succeeds: true,
					});
				} catch (err) {
					successful = false;
					if (err.message !== '405 Method Not Allowed') {
						throw err;
					}
					await sleep(1000);
				}

				if (successful) {
					break;
				}
			}
		} else {
			log.info(`${ packageJson.name }: No changes.`);
		}
	};

	// Delete temporary ssh key.
	let page = 0;
	while (true) {
		const keys = await gitlab.listUserKeys({
				page,
			}),
			temporaryKey = keys.find(key => key.title === publicKeyLabel);
		
		if (temporaryKey) {
			await gitlab.removeUserKey(temporaryKey.id);
			break;
		} else if (keys.length === 0) {
			log.error(`Could not delete key ${ publicKeyLabel }.`);
			break;
		} else {
			++page;
		}
	}
};

(async() => {
	try {
		await integrate();
	} catch (err) {
		process.exitCode = 1;
		throw err;
	}
})();
