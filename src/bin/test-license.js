/*global __dirname,process,Promise,require*/
import checker from 'license-checker';
import hasProperty from '@megatherium/has-property';
import logger from './logger';
import minimatch from 'minimatch';
import path from 'path';

const log = logger.init('test-license.js');

(async() => {
	const config = require('../../.licenserc');

	await new Promise((resolve, reject) => {
		checker.init({
			exclude: config.licenses.join(','),
			start: path.join(__dirname, '..', '..'),
		}, (err, packages) => {
			if (err) {
				return reject(err);
			}

			const missing = {};
			for (const name in packages) {
				const licenses = Array.isArray(packages[name].licenses)
						? packages[name].licenses
						: [
							packages[name].licenses,
						],
					missingLicenseIndex = licenses.findIndex((license) => hasProperty(missing, license));

				if (missingLicenseIndex < 0
					&& licenses.findIndex((license) => {
						return config.licenses.indexOf(license) >= 0;
					}) < 0
					&& config.modules.findIndex((moduleName) => {
						return minimatch(name, moduleName);
					}) < 0) {
					licenses.forEach((license) => {
						missing[license] = [
							name,
						];
					});
				} else if (missingLicenseIndex >= 0) {
					missing[licenses[missingLicenseIndex]].push(name);
				}
			}

			if (Object.keys(missing).length > 0) {
				log.error('Missing licenses', missing);
				process.exit(1);
			}

			log.info('All licenses valid.');
		});
	});
})();
