import log from '@megatherium/log';

const logger = log.init({
	app: '@megatherium/bench',
});

export default logger;
