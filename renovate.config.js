/*global module,process*/
// eslint-disable-next-line import/no-commonjs
module.exports = {
	endpoint: 'https://gitlab.com/api/v4/',
	platform: 'gitlab',
	repositories: [
		'megatherium/bench',
	],
	token: process.env.MEGATHERIUM_GITLAB_ACCESS_TOKEN || process.env.GITLAB_TOKEN,
};
