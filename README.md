# bench

Returns a benchmarking function.

See the whole [documentation](https://megatherium.gitlab.io/bench) or the [coverage report](https://megatherium.gitlab.io/bench/coverage).

## Getting started

Install the module:

`$ npm install @megatherium/bench`

Use the module:

```
import assert from 'assert';
import bench from '@megatherium/bench';

const mark = bench('Sleeping 2 seconds');
setTimeout(() => {
	const timeInMilliseconds = mark();
	assert.strictEqual(timeInMilliseconds >= 2000, true);
}, 2000);
```

## API (1)

### Exports

[bench](module-bench.html)`(label?: string, customLog?: log.Logger): Function` Returns a benchmarking-function. Calling the benchmarking-function will return the time in milliseconds since it's creation and print a benchmark in the `debug`-channel.

### Scripts

The following scripts can be executed using `npm run`:
- `build` Builds the module.
- `build-docs` Builds the documentation.
- `build-source` Builds the source code.
- `build-tests` Builds test-cases from jsdoc examples.
- `clear` Clears the module from a previous build.
- `clear-coverage` Clears the coverage reports and caches.
- `clear-docs` Clears the previous documentation build.
- `clear-source` Clears the previous source build.
- `clear-tests` Clears the generated jsdoc example test files.
- `fix` Runs all automated fixes.
- `fix-lint` Automatically fixes linting problems.
- `release` Runs semantic release. Meant to be only executed by the CI, not by human users.
- `test` Executes all tests.
- `test-coverage` Generates coverage reports from the test results using [nyc](https://www.npmjs.com/package/nyc).
- `test-deps` Executes a [depcheck](https://www.npmjs.com/package/depcheck).
- `test-e2e` Executes End-to-End-Tests using [cucumber](https://github.com/cucumber/cucumber-js).
- `test-integration` Executes integration tests using [jest](https://jestjs.io/).
- `test-lint` Executes linting tests using [eslint](https://eslint.org/).
- `test-unit` Executes unit tests using [mocha](https://mochajs.org/).
- `update` Checks for dependency updates using [renovate](https://www.npmjs.com/package/renovate).

## Contribution

See [Contribution Guidelines](CONTRIBUTION.md) for more details.

